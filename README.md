# README #

This is a Web Application repository for my MOOC course.

### What is this repository for? ###

* Blog Web Application in Ruby on Rails
* Iteration 5

### How do I get set up? ###

* Checkout and run through rails

### Who do I talk to? ###

* [Arockia Anand Raj D](mailto:arockiaanandraj@hotmail.com)